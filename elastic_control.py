import os, json, sys, logging, datetime
from elasticsearch import Elasticsearch
from elasticsearch import helpers

class elasticsearch_control:

    debug = False
    es_host = None
    es_port = None
    es = None

    general_index_name = None
    taxa_index_name = None
    names_index_name = None
    active_index = None

    control_index_name = None
    logfile_path = None
    logger = None
    control_command = None
    control_argument = None

    def initialize(self):
        self.set_debug(os.getenv('DEBUGGING')=="1")

        for item in [ "ES_GENERAL_INDEX", "ES_TAXON_INDEX", "ES_NAMES_INDEX", "ES_CONTROL_INDEX", "ES_HOST", "ES_PORT", "LOGFILE_PATH" ]:
            if (os.getenv(item)==None):
                raise ValueError("'{}' not set in ENV".format(item))

        self.initialize_logger(log_level=logging.DEBUG if self.get_debug() else logging.INFO)
        self.initialize_elastic()

    def initialize_logger(self,log_level=logging.INFO):
        self.logger=logging.getLogger("loader")
        self.logger.setLevel(log_level)

        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        fh = logging.FileHandler(os.getenv('LOGFILE_PATH'))
        fh.setLevel(log_level)
        fh.setFormatter(formatter)
        self.logger.addHandler(fh)
        
        if self.get_debug():
            ch = logging.StreamHandler()
            ch.setLevel(log_level)
            ch.setFormatter(formatter)
            self.logger.addHandler(ch)

    def initialize_elastic(self):
        self.es_host=os.getenv('ES_HOST')
        self.es_port=os.getenv('ES_PORT')
        self.general_index_name = os.getenv('ES_GENERAL_INDEX')
        self.taxa_index_name = os.getenv('ES_TAXON_INDEX')
        self.names_index_name = os.getenv('ES_NAMES_INDEX')
        self.control_index_name = os.getenv('ES_CONTROL_INDEX')

        self.es = Elasticsearch([{'host': self.es_host, 'port': self.es_port}])

        self.logger.debug("elastic host: {}".format(self.es_host))
        self.logger.debug("elastic port: {}".format(self.es_port))
        self.logger.debug("elastic index general: {}".format(self.general_index_name))
        self.logger.debug("elastic index taxa: {}".format(self.taxa_index_name))
        self.logger.debug("elastic index names: {}".format(self.names_index_name))
        self.logger.debug("elastic index control: {}".format(self.control_index_name))
 
    def set_debug(self,state):
        self.debug=state

    def get_debug(self):
        return self.debug

    def check_availability(self):
        try:
            self.es.info()
            self.logger.info("elasticsearch available")
        except:
            self.logger.error("elasticsearch unavailable")

    def delete_index(self,index):
        try:
            result = self.es.indices.delete(index=index)
            self.logger.info(json.dumps(result))
        except Exception as e:
            self.logger.error(e)

    def create_index_from_file(self,mapping_file,index):
        try:
            with open(mapping_file, 'r') as file:
                # doc = file.read().replace('\n', ' ')
                doc = json.loads(file.read())

            result = self.es.indices.create(index=index, body=doc)
            self.logger.info(json.dumps(result))
        except Exception as e:
            self.logger.error(e)

    def load_documents_from_folder_one_by_one(self,folder,index):
        loaded = 0
        failed = 0
        for filename in os.listdir(folder):
            if filename.endswith(".json"):
                try:
                    file = open(os.path.join(folder,filename))
                    doc = json.loads(file.read())
                    result = self.es.index(index=index, id=doc["id"], body=doc, op_type='create')
                    self.logger.info("loaded: {}".format(filename))
                    loaded += 1
                except Exception as e:
                    self.logger.error("error loading: {} ({})".format(filename,e))
                    failed += 1

        self.logger.info("finished loading: {}; successful {}, failed {}".format(folder,loaded,failed))

    def folder_reader(self,folder):
        for filename in os.listdir(folder):
            if filename.endswith(".json"):
                file = open(os.path.join(folder,filename))
                doc = json.loads(file.read())
                yield doc["id"], doc

    def load_documents_from_folder(self,folder,index):
        k = ({
                "_index": index,
                "_type" : "_doc",
                "_id"   : idx,
                "_source": doc,
             } for idx, doc in self.folder_reader(folder))

        r = helpers.bulk(self.es, k)
        self.es.indices.flush(index)
        self.logger.info("finished loading: {}; {}".format(folder,r))


    def delete_document_by_id(self,doc_id,index):
        result = self.es.delete(index=index, id=doc_id, refresh=True, ignore=[400, 404])
        self.logger.info(json.dumps(result))

    def delete_document_by_category(self,category,index):
        self.delete_documents_by_query(index,'{{ "query": {{ "term" : {{ "category" : "{}" }}  }} }}'.format(category))

    def delete_documents_by_query(self,index,query='{"query":{"match_all": {} } }'):
        result = self.es.delete_by_query(index=index, body=query, refresh=True,timeout="5m")
        self.logger.info(json.dumps(result))

    def set_api_status(self,state):
        doc = '{{  "status": "{}", "created":  "{}" }}'

        if state in [ "busy", "ready" ]:
            date = datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%S')
            query = doc.format(state, date)
            result = self.es.index(index=self.control_index_name, id="1", body=query)
            self.logger.info("set documents state: {}".format(state))
        else:
            self.logger.warning("unknown state: {}".format(state))

    def get_api_status(self):
        response = self.es.search(index=self.control_index_name,body='{}')
        try:
            documents_status=response["hits"]["hits"][0]["_source"]["status"]
        except Exception as e:
            self.logger.warning("document status unavailable (defaulting to 'ready')")
            documents_status="ready"
        return documents_status

    def set_active_index(self,index):
        if index == None:
            return

        self.active_index = None

        for item in [ self.control_index_name, self.general_index_name, self.taxa_index_name, self.names_index_name ]:
            if item == index:
                self.active_index = index
                break

        if self.active_index == None:
            self.logger.warning("unknown index: {}".format(index))
        else:
            self.logger.info("active index: {}".format(self.active_index))

    def set_control_command(self,command):
        if command == None:
            return

        if command in [ "create_index", "delete_index", "create_control_index", "delete_control_index", "load_documents",
                        "delete_document", "delete_documents", "set_api_status", "delete_category", "get_api_status" ]:
            self.control_command = command
        else:
            self.control_command = None
            self.logger.warning("unknown control command: {}".format(command))

        self.logger.debug("control command: {}".format(self.control_command))

    def set_control_argument(self,argument):
        self.control_argument = argument
        self.logger.debug("control argument: {}".format(self.control_argument))

    def run_control_command(self):
        if self.control_command=='create_index' and not self.control_argument==None and not self.active_index==None:
            if not os.path.exists(self.control_argument):
                self.logger.warning("file doesn't exist: {}".format(self.control_argument))
            else:
                self.logger.info("creating index from file: {}".format(self.control_argument))
                self.create_index_from_file(self.control_argument,index=self.active_index)
            return

        if self.control_command=='delete_index' and not self.active_index==None:
            self.logger.info("deleting index")
            self.delete_index(index=self.active_index)
            return

        if self.control_command=='load_documents' and not self.control_argument==None and not self.active_index==None:
            if not os.path.exists(self.control_argument):
                self.logger.warning("folder doesn't exist: {}".format(self.control_argument))
            else:
                self.logger.info("loading documents from folder: {}".format(self.control_argument))
                self.load_documents_from_folder(self.control_argument,self.active_index)
            return

        if self.control_command=='delete_documents' and not self.active_index==None:
            self.logger.info("deleting all documents")
            self.delete_documents_by_query(self.active_index)
            return

        if self.control_command=='delete_document' and not self.control_argument==None and not self.active_index==None:
            self.logger.info("delete document by id: {}".format(self.control_argument))
            self.delete_document_by_id(self.control_argument,self.active_index)
            return

        if self.control_command=='set_api_status' and not self.control_argument==None:
            self.set_api_status(self.control_argument)
            return

        if self.control_command=='get_api_status':
            status = self.get_api_status()
            self.logger.info("API status: {}".format(status))
            return status

        if self.control_command=='delete_category' and not self.control_argument==None and not self.active_index==None:
            self.logger.info("delete document by category: {}".format(self.control_argument))
            self.delete_document_by_category(self.control_argument,self.active_index)
            return

if __name__ == '__main__':

    e = elasticsearch_control()
    e.initialize()
    e.check_availability()
    e.set_active_index(os.getenv('ES_ACTIVE_INDEX') if not os.getenv('ES_ACTIVE_INDEX')==None else None)
    e.set_control_command(os.getenv('ES_CONTROL_COMMAND') if not os.getenv('ES_CONTROL_COMMAND')==None else None)
    e.set_control_argument(os.getenv('ES_CONTROL_ARGUMENT') if not os.getenv('ES_CONTROL_ARGUMENT')==None else None)
    e.run_control_command()

